package com.example.bitcointransactionsmapper.client

import com.example.bitcointransactionsmapper.BuildConfig
import com.example.bitcointransactionsmapper.domain.BitCoinToUsdRate
import retrofit2.http.GET
import retrofit2.http.Query

interface CryptoRateClient {
    @GET("data/price")
    fun getBitcoinToUsdRate( @Query("fsym") fsys : String,
                             @Query("tsyms") tsys : String,
                             @Query("api_key") apiKey : String,
                             ): retrofit2.Call<BitCoinToUsdRate>
}