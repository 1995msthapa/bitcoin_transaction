package com.example.bitcointransactionsmapper.domain

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class BitCoinToUsdRate (
    @Json(name = "USD")
     var usd : Double
        )
