package com.example.bitcointransactionsmapper.Utils

import android.R
import android.app.Activity
import android.content.Context
import android.view.View
import com.google.android.material.snackbar.Snackbar

object Utils {
    const val SATOSHI_PER_BITCOIN = 100000000
    const val TSYS = "USD"
    const val FSYS = "BTC"
    var bitCoinToUsdRate: Double = 0.0


    fun getUSDFromSatoshi(satoshi : Double) : Double {
        return ((satoshi)/ SATOSHI_PER_BITCOIN)*bitCoinToUsdRate
    }

    fun showSnackBar(view: View?, context: Context, msg: String?) {
        view?.let {
            Snackbar.make(it, msg?:"", Snackbar.LENGTH_INDEFINITE)
                .setAction("CLOSE") { }
                .setActionTextColor(
                    context.resources.getColor(R.color.white)
                ).setBackgroundTint(
                    when(msg) {
                    context.getString(com.example.bitcointransactionsmapper.R.string.msg_connecting)
                    ->  context.resources.getColor(R.color.holo_purple)
                    context.getString(com.example.bitcointransactionsmapper.R.string.msg_connected)
                    ->  context.resources.getColor(R.color.holo_green_light)
                    context.getString(com.example.bitcointransactionsmapper.R.string.msg_closed),
                    context.getString(com.example.bitcointransactionsmapper.R.string.msg_something_went_wrong),
                    ->  context.resources.getColor(R.color.holo_red_light)
                    else -> context.resources.getColor(R.color.background_dark)
                }). setTextColor(
                     context.resources.getColor(R.color.white)
                    ).show()

        }

    }


}

