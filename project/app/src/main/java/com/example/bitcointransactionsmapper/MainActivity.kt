package com.example.bitcointransactionsmapper

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.bitcointransactionsmapper.Utils.Utils
import com.example.bitcointransactionsmapper.adapter.TransactionAdapter
import com.example.bitcointransactionsmapper.databinding.ActivityMainBinding

import com.example.bitcointransactionsmapper.domain.TransactionResponse
import com.example.bitcointransactionsmapper.viewModel.ActivityViewModel

import java.net.URI

class MainActivity : AppCompatActivity() {
    var binding : ActivityMainBinding ?= null
    var viewModel : ActivityViewModel ?= null
    var data : TransactionResponse ?= null
    var transactionResponse : TransactionResponse ?= null
    var mAdapter : TransactionAdapter ?= null
    var listTransactions = ArrayList<TransactionResponse>()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this).get(ActivityViewModel::class.java)
        observeResponse()
        initializeRecyclerView()
        onClicklisteners()
        setContentView(binding?.root)

    }

    private fun onClicklisteners() {
        binding?.btnClear?.setOnClickListener {
            listTransactions?.clear()
            mAdapter?.setTransactionData(listTransactions)
            if(viewModel?.webSocketClientBlockchain?.isOpen == true)
            viewModel?.webSocketClientBlockchain?.close()

        }

        binding?.btnReload?.setOnClickListener {
            if(viewModel?.webSocketClientBlockchain?.isClosed == true)
            initWebSocket()
        }
    }

    private fun initializeRecyclerView() {

        mAdapter = TransactionAdapter(this)
        binding?.transactionList?.adapter  = mAdapter
        binding?.transactionList?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }

    private fun observeResponse() {
        viewModel?.transactionResponse?.observe(this, Observer {
            it?.let {  response ->
                transactionResponse = response
                viewModel?.getBitcoinRateInUsd()

            }
        })
        viewModel?.snackBarMessage?.observe(this, Observer {
        binding?.root?.let {  parent ->
            Utils.showSnackBar(parent, this, it)
        }

        })
        viewModel?.bitCoinToUsd?.observe(this, Observer {
            it?.let { bitCoinUsdRate ->
                Utils.bitCoinToUsdRate = bitCoinUsdRate
                transactionResponse?.let { transaction ->
                    transactionResponse?.x?.inputs?.get(0)?.prevOut?.value?.let {  satoshi ->
                        if( bitCoinUsdRate != 0.0 && ( Utils.getUSDFromSatoshi(satoshi  )).toInt() > 100){
                            if(listTransactions.size >=5){
                                while (listTransactions.size >= 5){
                                    listTransactions.removeAt(0)
                                }
                            }
                            listTransactions.add(transaction)
                            mAdapter?.setTransactionData(listTransactions)
                        }
                }

            }
        }})
    }

    override fun onResume() {
        super.onResume()
        initWebSocket()
    }

    private fun initWebSocket() {
        val coinbaseUri: URI? = URI(com.example.bitcointransactionsmapper.BuildConfig.BLOCKCHAIN_SERVER_URL)
        viewModel?.createTransactionWebSocketClient(coinbaseUri, this)
    }

    override fun onPause() {
        super.onPause()
        viewModel?.webSocketClientBlockchain?.close()

    }


}