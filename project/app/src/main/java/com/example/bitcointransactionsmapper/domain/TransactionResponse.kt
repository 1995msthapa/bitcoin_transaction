package com.example.bitcointransactionsmapper.domain

import androidx.lifecycle.GeneratedAdapter
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TransactionResponse(
    @Json(name = "op")
    var op: String?,
    @Json(name = "x")
    var x: X?
)

@JsonClass(generateAdapter = true)
data class X(
    @Json(name = "hash")
    var hash: String?,
    @Json(name = "inputs")
    var inputs: List<Input>?,
    @Json(name = "lock_time")
    var lockTime: Int?,
    @Json(name = "out")
    var `out`: List<Out>?,
    @Json(name = "relayed_by")
    var relayedBy: String?,
    @Json(name = "size")
    var size: Long?,
    @Json(name = "time")
    var time: Int?,
    @Json(name = "tx_index")
    var txIndex: Int?,
    @Json(name = "ver")
    var ver: Int?,
    @Json(name = "vin_sz")
    var vinSz: Int?,
    @Json(name = "vout_sz")
    var voutSz: Int?
)

@JsonClass(generateAdapter = true)
data class Input(
    @Json(name = "prev_out")
    var prevOut: PrevOut?,
    @Json(name = "script")
    var script: String?,
    @Json(name = "sequence")
    var sequence: Long?
)

@JsonClass(generateAdapter = true)
data class Out(
    @Json(name = "addr")
    var addr: String?,
    @Json(name = "n")
    var n: Int?,
    @Json(name = "script")
    var script: String?,
    @Json(name = "spent")
    var spent: Boolean?,
    @Json(name = "tx_index")
    var txIndex: Int?,
    @Json(name = "type")
    var type: Int?,
    @Json(name = "value")
    var value: Int?
)

@JsonClass(generateAdapter = true)
data class PrevOut(
    @Json(name = "addr")
    var addr: String?,
    @Json(name = "n")
    var n: Int?,
    @Json(name = "script")
    var script: String?,
    @Json(name = "spent")
    var spent: Boolean?,
    @Json(name = "tx_index")
    var txIndex: Int?,
    @Json(name = "type")
    var type: Int?,
    @Json(name = "value")
    var value: Double?
)