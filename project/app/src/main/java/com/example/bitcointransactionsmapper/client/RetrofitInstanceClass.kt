package com.example.bitcointransactionsmapper.client

import com.example.bitcointransactionsmapper.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


object RetrofitInstanceClass {

    var cryptoRateClient : CryptoRateClient ?= null

    fun getRetrofitClient() : CryptoRateClient {
        cryptoRateClient?.let {
            return it
        }
      cryptoRateClient =  Retrofit.Builder().
      baseUrl(BuildConfig.CRYPTOCOMPARE_SERVER_URL).addConverterFactory(MoshiConverterFactory.create())
            .build().create(CryptoRateClient::class.java)

        return cryptoRateClient!!
    }

}