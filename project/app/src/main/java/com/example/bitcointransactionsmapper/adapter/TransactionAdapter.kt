package com.example.bitcointransactionsmapper.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.bitcointransactionsmapper.R
import com.example.bitcointransactionsmapper.Utils.Utils
import com.example.bitcointransactionsmapper.databinding.ItemTransactionBinding
import com.example.bitcointransactionsmapper.domain.TransactionResponse
import java.text.SimpleDateFormat
import kotlin.collections.ArrayList

class TransactionAdapter(context : Context) : RecyclerView.Adapter<TransactionAdapter.ItemTransactionViewHolder>() {

 private var transactionList = ArrayList<TransactionResponse>()
   lateinit var binding : ItemTransactionBinding
   var mContext = context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemTransactionViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_transaction,
            parent,
            false
        )
        return ItemTransactionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemTransactionViewHolder, position: Int) {
        holder.bind(transactionList[position])
    }

    override fun getItemCount(): Int {
        return transactionList.size
    }

    fun setTransactionData( transactionList: ArrayList<TransactionResponse>){
        this.transactionList = transactionList
        notifyDataSetChanged()

    }
    inner class ItemTransactionViewHolder(var binding : ItemTransactionBinding) :
        RecyclerView.ViewHolder(binding.root ) {

            fun bind(itemTransaction : TransactionResponse){

                binding?.tvTime.text = mContext.getString(R.string.transaction_time,
                    itemTransaction.x?.time?.let {
                        SimpleDateFormat("dd-MM-yyyy : HH:mm:ss").format(it)
                    } ?: "")

                binding.tvHash.text = mContext.getString(R.string.transaction_hash,
                    itemTransaction?.x?.hash ?: "")

                binding.tvTranAmount.text =  itemTransaction?.x?.inputs?.get(0)?.prevOut?.value?.let { satoshi ->
                    mContext.getString(R.string.transaction_Amount,
                        (Utils.getUSDFromSatoshi(satoshi)).toString() ?: "")
                }

            }

    }
}