package com.example.bitcointransactionsmapper.viewModel

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bitcointransactionsmapper.BuildConfig
import com.example.bitcointransactionsmapper.R
import com.example.bitcointransactionsmapper.Utils.Utils
import com.example.bitcointransactionsmapper.client.RetrofitInstanceClass
import com.example.bitcointransactionsmapper.domain.BitCoinToUsdRate
import com.example.bitcointransactionsmapper.domain.TransactionResponse
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import org.java_websocket.server.DefaultSSLWebSocketServerFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.net.URI
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory

class ActivityViewModel : ViewModel() {

    var snackBarMessage = MutableLiveData<String>()
    var transactionResponse = MutableLiveData <TransactionResponse> ()
    var bitCoinToUsd = MutableLiveData<Double>()

    var webSocketClientBlockchain : WebSocketClient?= null

    fun createTransactionWebSocketClient(blockchainBaseURI: URI?, context : Activity) {
        snackBarMessage.postValue(context.getString(R.string.msg_connecting))
        webSocketClientBlockchain = object : WebSocketClient(blockchainBaseURI) {
            override fun onOpen(handshakedata: ServerHandshake?) {
                snackBarMessage.postValue(context.getString(R.string.msg_connected))
                subscribeBlockChain()


            }

            override fun onMessage(message: String?) {
                context.runOnUiThread {
                    postTransactionResponse(message) }


            }

            override fun onClose(code: Int, reason: String?, remote: Boolean) {
                snackBarMessage.postValue(context.getString(R.string.msg_closed))
                          unSubscribeBlockChain()
            }

            override fun onError(ex: Exception?) {
                snackBarMessage.postValue(context.getString(R.string.msg_something_went_wrong))
            }

        }

        val socketFactory: SSLSocketFactory = SSLSocketFactory.getDefault() as SSLSocketFactory
        webSocketClientBlockchain?.setSocketFactory(socketFactory)
        webSocketClientBlockchain?.connect()



    }

    fun getBitcoinRateInUsd() {
        RetrofitInstanceClass.getRetrofitClient().getBitcoinToUsdRate(Utils.FSYS, Utils.TSYS,
        BuildConfig.CRYPTOCOMPARE_API_KEY).enqueue(
            object : Callback<BitCoinToUsdRate> {
                override fun onResponse(
                    call: Call<BitCoinToUsdRate>,
                    response: Response<BitCoinToUsdRate>
                ) {
                    response?.body()?.usd?.let {
                        bitCoinToUsd.postValue(it)
                    }

                }

                override fun onFailure(call: Call<BitCoinToUsdRate>, t: Throwable) {
                    snackBarMessage.postValue("Something went wrong!")
                }

            })

    }

    fun postTransactionResponse(message : String?) {
        message?.let {
            try {
                val moshi = Moshi.Builder().build()
                val adapter: JsonAdapter<TransactionResponse> =
                    moshi.adapter(TransactionResponse::class.java)
                transactionResponse.postValue(adapter.fromJson(message))
            }catch (e : Exception){
                e.printStackTrace()
            }

        }
    }



    fun subscribeBlockChain() {
        webSocketClientBlockchain?.send(
            "{\n" +
                    "  \"op\": \"unconfirmed_sub\"\n" +
                    "}"
        )
    }

     fun unSubscribeBlockChain() {
        webSocketClientBlockchain?.send(
            "{\n" +
                    " \"op\": \"unconfirmed_unsub\",\n" +
                    "}"
        )
    }

}

